export class DocumentsEnvironment {
  unstableApiKey: string = "";
}

export class FormReference {
  clientApiIdentifier: string = "";
  libraryApiIdentifier: string = "";
  formApiIdentifier: string = "";
  formVersionId: string = "";
}

export class StagedContent {
  stagedContentId: string = "";
  stagedContentType: string = "";
}

export class AddDocumentTemplateToFormRequest {
  env: DocumentsEnvironment = new DocumentsEnvironment();
  formReference: FormReference = new FormReference();
  stagedContent: StagedContent = new StagedContent();
}
