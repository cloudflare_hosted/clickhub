import axios, { type AxiosResponse } from "axios";
import { useAuthStore } from "@/stores/auth";
import { Refresh } from "@/services/clickHubService";

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_API_BASE_URL,
});

axiosInstance.interceptors.request.use((config) => {
  const auth = useAuthStore();
  const token = auth.accessToken;
  if (token) {
    config.headers["Authorization"] = "Bearer " + token;
  }
  return config;
});

axiosInstance.interceptors.response.use(
  async (response) => {
    return response;
  },
  async (error) => {
    const auth = useAuthStore();
    const originalRequest = error.config;
    if (error.response?.status === 401) {
      console.log("failed auth");
      const refresh = auth.refreshToken;
      if (refresh) {
        const res: AxiosResponse<LoginPto> = await Refresh(refresh);
        if (res.status === 200) {
          auth.setAuthValues(res?.data);
          originalRequest.headers["Authorization"] =
            "Bearer " + auth.accessToken;
          return axios.request(originalRequest);
        } else {
          console.log("should logout");
          auth.logout();
        }
      } else {
        console.log("had no refresh");
        auth.logout();
      }
    }
    return Promise.reject(error);
  },
);
export const useHttp = () => axiosInstance;

type LoginPto = {
  accessToken: string;
  refreshToken: string;
};
