import { createApp } from "vue";
import { createPinia } from "pinia";
import "@/assets/styles/main.scss";
import Particles from "@tsparticles/vue3";
import { loadFull } from "tsparticles";
import breadcrumbs from "vue-3-breadcrumbs";

import App from "./App.vue";
import router from "./router";

const app = createApp(App);

app
  .use(createPinia())
  .use(router)
  .use(breadcrumbs, { includeComponent: true })
  .use(Particles, {
    init: async (engine: any) => {
      await loadFull(engine);
    },
  })
  .mount("#app");
export { app };
