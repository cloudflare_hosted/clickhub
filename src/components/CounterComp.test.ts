import { useCounterStore } from "./../stores/counter";
import { describe, it, expect, beforeEach, test } from "vitest";
import { mount } from "@vue/test-utils";
import CounterComp from "./CounterComp.vue";
import { createPinia, setActivePinia } from "pinia";

function mountComponent() {
  const wrapper = mount(CounterComp);
  return wrapper;
}

describe("Component", () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });
  it("Mounts correctly", () => {
    const counter = useCounterStore();
    expect(mountComponent()).toBeTruthy();
  });

  test("Button increases pinia store count", async () => {
    const counter = useCounterStore();
    const button = mountComponent().find("button");
    await button.trigger("click");
    expect(counter.count).toBe(1);
  });
});
