import axios from "axios";
import type { App } from "vue";
// Axios

axios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;
export default (app: App) => {
  (app as any).axios = axios;
  (app as any).$http = axios;

  app.config.globalProperties.axios = axios;
  app.config.globalProperties.$http = axios;
};
