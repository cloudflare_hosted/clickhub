import { createRouter, createWebHistory } from "vue-router";
import HomeView from "@/views/HomeView.vue";
import AboutView from "@/views/AboutView.vue";
import LoginView from "@/views/LoginView.vue";
import ReportsView from "@/views/InternalReportsView.vue";
import CandidateCountView from "@/views/InternalReports/CandidateCountView.vue";
import ActiveLocationsView from "@/views/InternalReports/ActiveLocationsView.vue";
import { useAuthStore } from "@/stores/auth";
import { Refresh } from "@/services/clickHubService";
import SymmetryCompletionsView from "@/views/InternalReports/SymmetryCompletionsView.vue";
import ClientsView from "@/views/ClientsView.vue";
import { fourOhFour } from "@/views/misc";
const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      redirect: "clients",
    },
    {
      path: "/clients",
      name: "clients",
      component: ClientsView,
      meta: {
        auth: true,
        breadcrumb: {
          label: "Clients",
        },
      },
    },

    {
      path: "/login",
      name: "login",
      component: LoginView,
    },
    {
      path: "/migrateGlobalForms",
      name: "migrateGlobalForms",
      component: () => import("../views/MigrateGlobalForms.vue"),
      meta: {
        auth: true,
        breadcrumb: {
          label: "Migrate",
        },
      },
    },
    {
      path: "/internalReports",
      name: "internalReports",
      component: ReportsView,
      meta: {
        auth: true,
        breadcrumb: {
          label: "Internal Reports",
        },
      },
    },
    {
      path: "/internalReports/candidateCounts",
      name: "candidateCounts",
      component: CandidateCountView,
      meta: {
        auth: true,
        breadcrumb: {
          label: "Candidate Counts",
        },
      },
    },
    {
      path: "/internalReports/activeLocations",
      name: "activeLocations",
      component: ActiveLocationsView,
      meta: {
        auth: true,
        breadcrumb: {
          label: "Active Locations",
        },
      },
    },
    {
      path: "/internalReports/symmetryCompletions",
      name: "symmetryCompletions",
      component: SymmetryCompletionsView,
      meta: {
        auth: true,
        breadcrumb: {
          label: "Symmetry Completions",
        },
      },
    },
    {
      name: "Page not found",
      path: "/:pathMatch(.*)*",
      component: fourOhFour,
      meta: {
        auth: false,
        breadcrumb: {
          label: "Page not found",
        },
      },
    },
  ],
});

router.beforeEach(async (to, from, next) => {
  if (typeof to.name === "string") {
    appInsights.startTrackPage(to.name);
  }
  if (typeof from.name === "string") {
    appInsights.stopTrackPage(from.name, from.fullPath);
  }
  const auth = useAuthStore();
  if (to.meta.auth) {
    try {
      await authCheck().then(() => {
        next();
      });
    } catch {
      console.error("failed to use auth");
      auth.logout();
    }
  } else {
    next();
  }
});
export default router;

export default router;
