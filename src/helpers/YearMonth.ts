import { ref, computed } from "vue";

type FormatMonth = (month: number) => string;
export const formatMonth: FormatMonth = (month: number) => {
  const p1 = month++;
  if (p1 < 10) return `0${p1}`;
  else return `${p1}`;
};

export const today = () => {
  const date = new Date();
  return `${date.getFullYear()}-${formatMonth(date.getMonth())}`;
};

type AddAMonth = (my: string) => string;
export const addAMonth: AddAMonth = (my) => {
  const [year, month] = my.split("-");
  const numYear = Number(year);
  const numMonth = Number(month);
  if (numMonth === 12) return `${numYear + 1}-01`;
  else
    return `${year}-${numMonth + 1 >= 10 ? numMonth + 1 : `0${numMonth + 1}`}`;
};

export const currentYm = computed(() => `${new Date().getFullYear()}-01`);

type DateParts = (yearMonthString: string) => { tm: string; nm: string };
export const dateParts: DateParts = (yearMonthString) => {
  const thisMonth = new Date(yearMonthString);
  const lastMonth = new Date(addAMonth(yearMonthString));
  return {
    tm: thisMonth.toISOString(),
    nm: lastMonth.toISOString(),
  };
};
