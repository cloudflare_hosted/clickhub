export type Option<T> = Some<T> | None;
export interface Some<T> {
  _tag: "Some";
  value: T;
}
export interface None {
  _tag: "None";
}

export const some = <T>(x: T): Option<T> => ({ _tag: "Some", value: x });
export const none: Option<never> = { _tag: "None" };

export const isNone = <T>(x: Option<T>): x is None => x._tag === "None";
