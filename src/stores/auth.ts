// stores/counter.js
import { defineStore } from "pinia";
import { ref, computed } from "vue";

export const useAuthStore = defineStore("auth", () => {
  const userLoggedIn = ref("false");
  const authLocal = new Map([["userLoggedIn", userLoggedIn]]);

  //sets a linked pinia and local storage item
  async function setItem({
    itemName,
    itemValue,
  }: {
    itemName: string;
    itemValue: string;
  }) {
    localStorage.setItem(itemName, itemValue);
    const mapItem = authLocal.get(itemName);
    if (mapItem) {
      mapItem.value = itemValue;
    }
  }

  //gets and item from local storage
  function getItem(itemName: string): string | null {
    return localStorage.getItem(itemName);
  }
  //syncs what is in local storage with what is in the pina store
  // on reload
  function getFromStorage() {
    authLocal.forEach((value, key) => {
      const storedValue = getItem(key);
      if (storedValue) value.value = storedValue;
      else console.error(`No found stored value of ${key}`);
    });
  }

  const accessToken = ref();
  const refreshToken = ref();
  const cookieJar = new Map([
    ["access", accessToken],
    ["refresh", refreshToken],
  ]);
  // makes a cookie that is synced with a pinia store value
  async function setCookie({
    itemName,
    itemValue,
    expires,
  }: {
    itemName: string;
    itemValue: string;
    expires?: number;
  }) {
    document.cookie = `${itemName}=${itemValue};${
      expires
        ? `expires=${new Date(
            new Date().getTime() + expires * 1000,
          ).toUTCString()}`
        : ""
    }`;
    const mapItem = cookieJar.get(itemName);
    if (mapItem) {
      mapItem.value = itemValue;
    }
  }
  // returns a cookie
  function getCookie(itemName: string): string | null {
    const matches = document.cookie.match(
      new RegExp(
        "(?:^|; )" +
          itemName.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "$1") +
          "=([^;]*)",
      ),
    );
    return matches ? decodeURIComponent(matches[1]) : null;
  }
  // resyncs the pinia store value with the cookie on reload
  function getCookiesFromStorage() {
    cookieJar.forEach((value, key) => {
      const storedValue = getCookie(key);
      if (storedValue !== null) value.value = storedValue;
      else {
        console.error(`no stored cookie called ${key} found`);
        value.value = null;
      }
    });
  }

  function setAuthValues(ld: any) {
    if (ld.accessToken) {
      setCookie({
        itemName: "access",
        itemValue: ld.accessToken,
        expires: ld.expiresIn || 3600,
      });
    }
    if (ld.refreshToken) {
      setCookie({ itemName: "refresh", itemValue: ld.refreshToken });
    }
    setItem({ itemName: "userLoggedIn", itemValue: "true" });
  }

  const readyToLoad = computed(() => {
    if (
      userLoggedIn.value === "true" &&
      accessToken.value &&
      !authLoading.value
    ) {
      return true;
    } else {
      return false;
    }
  });

  const authLoading = ref(false);

  async function authCheck() {
    authLoading.value = true;
    if (!haveAccessToken.value) {
      const refresh = getCookie("refresh");
      if (refresh !== null) {
        await UserRefresh(refresh);
        authLoading.value = false;
        return Promise.resolve;
      } else {
        if (window.location.pathname !== "/login") {
          throw "logging out";
        }
      }
    } else {
      authLoading.value = false;
      return Promise.resolve;
    }
  }

  function logout() {
    setItem({ itemName: "userLoggedIn", itemValue: "false" });
    //window.location.href = '/login'
    localStorage.clear();
  }

  getCookiesFromStorage();
  getFromStorage();
  return {
    userLoggedIn,
    logout,
    setAuthValues,
    accessToken,
    refreshToken,
    readyToLoad,
  };
});
