import { defineStore } from "pinia";

export const useDateStore = defineStore({
  id: "date",
  state: () => ({
    day: 1,
    month: 1,
    year: new Date().getFullYear(),
    endMonth: 1,
    endYear: new Date().getFullYear(),
  }),
  actions: {
    updateYear(year: number) {
      this.year = year;
    },
    updateMonth(month: number) {
      this.month = month;
    },
    setEndYear(year: number) {
      this.endYear = year;
    },
    setEndMonth(month: number) {
      this.endMonth = month;
    },
  },
});
