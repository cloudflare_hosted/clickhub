import { defineStore } from "pinia";
import { ref, computed } from "vue";
import type { AxiosError } from "axios";

export const useErrorStore = defineStore("error", () => {
  const errors = ref(new Map());

  type CrawlErrorForKey = ({
    name,
    errorObject,
  }: {
    name: string;
    errorObject: { [key: string]: any };
  }) => string;
  const crawlErrorForKey: CrawlErrorForKey = (args) => {
    const { name, errorObject } = args;
    if (errorObject[name]) return errorObject[name];
    if (errorObject.response[name]) return errorObject.response[name];
    if (errorObject.response.data[name]) return errorObject.response.data[name];
  };

  type TitleFromError = (e: AxiosError) => string;
  const titleFromError: TitleFromError = (e) =>
    crawlErrorForKey({ name: "title", errorObject: e });

  type MessageFromError = (e: AxiosError) => string;
  const messageFromError: MessageFromError = (e) =>
    crawlErrorForKey({ name: "message", errorObject: e });

  function logError(
    error: string | { axiosError: AxiosError; backup?: string },
  ) {
    const time = new Date().getTime();
    if (typeof error === "string") {
      errors.value.set(time, error);
    } else {
      errors.value.set(
        time,
        `${titleFromError(error.axiosError) || "Error!"}: ${
          messageFromError(error.axiosError) ||
          error.backup ||
          "Something whent wrong"
        }`,
      );
    }
  }

  function clearErrors() {
    errors.value = new Map();
  }

  const anyErrors = computed(() => errors.value.size > 0);

  return { errors, logError, clearErrors, anyErrors };
});

// ErrorViewContent: {
//   Default: {
//     icon: "emoji-frown",
//     iconDescription: "Frown",
//     title: "Oops!",
//     message:
//       "Something unexpected has happened. Please use the link below to go back and try again.",
//     showHomeButton: true,
//   },
//   Forbidden: {
//     icon: "shield-lock",
//     iconDescription: "Lock",
//     title: "Forbidden",
//     message:
//       "This page is valid but you do not have access to this content. Please contact an administrator to request access.",
//     showHomeButton: true,
//   },
//   BadLogin: {
//     icon: "shield-lock",
//     iconDescription: "Lock",
//     title: "Bad Login",
//     message: "Oops! Password or username incorrect",
//     showHomeButton: false,
//   },
//   NotConnecting: {
//     icon: "shield-lock",
//     iconDescription: "Lock",
//     title: "Network Error",
//     message: "Something is wrong with the network",
//     showHomeButton: false,
//   },
//   NotFound: {
//     icon: "signpost-split",
//     iconDescription: "Sign Post",
//     title: "Page Not Found",
//     message:
//       "We weren't able to locate the page you are looking for. Don't worry though, we can get you back on track with the link below.",
//     showHomeButton: true,
//   },
//   NoCompanyAccess: {
//     icon: "shield-lock",
//     iconDescription: "Lock",
//     title: "No Company Access",
//     message:
//       "Your company does not currently have access to the application, contact an administrator to learn more.",
//     showHomeButton: false,
//   },
// },
