import { useHttp } from "@/http";
import type { AxiosResponse } from "axios";

const apiBaseUrl = `${import.meta.env.VITE_API_BASE_URL}`;
const http = useHttp();

export async function getClients(): Promise<AxiosResponse> {
  try {
    const response: AxiosResponse = await http.get("/Clients");
    return response;
  } catch (error) {
    console.error(error);
    throw error;
  }
}
