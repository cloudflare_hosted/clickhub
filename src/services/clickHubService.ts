import { useHttp } from "@/http";
import type { AxiosResponse } from "axios";

const http = useHttp();

export async function SignedIn(): Promise<Number> {
  try {
    const response: AxiosResponse<Number> = await http.get("/Login/SignedIn");
    return response.status;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function Login(
  username: string,
  password: string,
): Promise<LoginRes> {
  try {
    const response: AxiosResponse<LoginRes> = await http.post("/Login/Native", {
      username,
      password,
    });
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function signedIn(token: string) {
  try {
    const response: AxiosResponse = await http.get("/Login/SignedIn", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return response.status;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function Refresh(refreshToken: string): Promise<AxiosResponse> {
  try {
    const response: AxiosResponse = await http.post("/Login/Refresh", {
      refreshToken,
    });
    return response;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function AdminLink(
  clientApiIdentifier: string,
  instance: string,
): Promise<AdminLinkRes> {
  try {
    const response: AxiosResponse<AdminLinkRes> = await http.get(
      `/login/adminLink?clientApiIdentifier=${clientApiIdentifier}&instance=${instance}`,
    );
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

type AdminLinkRes = {
  link: string;
};

interface LoginRes {
  refreshToken: string;
  accessToken: string;
}
