import { useHttp } from "@/http";
import type { AxiosResponse } from "axios";

const apiBaseUrl = `${import.meta.env.VITE_API_BASE_URL}`;
const http = useHttp();

export async function getCandidateCount(
  startDate: string,
  endDate: string,
  clientApiIdentifier: string,
): Promise<AxiosResponse> {
  try {
    const response: AxiosResponse = await http.get(
      `/Reports/${clientApiIdentifier}/CandidateCount?startDate=${startDate}&endDate=${endDate}`,
    );
    return response;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function getActiveLocations(
  startDate: string,
  endDate: string,
  clientApiIdentifier: string,
): Promise<AxiosResponse> {
  try {
    const response: AxiosResponse = await http.get(
      `/Reports/${clientApiIdentifier}/ActiveLocations?startDate=${startDate}&endDate=${endDate}`,
    );
    return response;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function getServiceFormCount(
  startDate: string,
  endDate: string,
  isSymmetry: boolean,
  isFormi9: boolean,
  skip: number,
  take: number,
): Promise<AxiosResponse> {
  try {
    const postBody = {
      filteredNoData: false,
      skip: skip,
      take: take,
    };

    const response: AxiosResponse = await http.post(
      `/ServiceForms?startDate=${startDate}&endDate=${endDate}&isSymmetry=${isSymmetry}&isFormi9=${isFormi9}`,
      postBody,
    );
    return response;
  } catch (error) {
    throw error;
  }
}
