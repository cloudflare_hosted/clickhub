import { useHttp } from "@/http";

const apiBaseUrl = `${import.meta.env.VITE_API_BASE_URL}`;

const http = useHttp();

export class GlobalFormModel {
  public sourceEnv: string = "";
  public sourceClientApiIdentifier: string = "";
  public sourcePublicApiKey: string = "";
  public sourceUnstableApiKey: string = "";
  public targetEnv: string = "";
  public targetClientApiIdentifier: string = "";
  public targetPublicApiKey: string = "";
  public targetUnstableApiKey: string = "";
}

export const supportedDocumentTemplates = [
  {
    extensions: ["html", "htm"],
    documentTemplateType: "DotLiquidHtml",
    contentType: "text/html",
  },
  {
    extensions: ["pdf"],
    documentTemplateType: "AcroForms",
    contentType: "application/pdf",
  },
];

export async function migrateAllGlobalForms(model: GlobalFormModel) {
  return (
    await http.post(`/FormManagement/migrateAllForms`, model).catch((err) => {
      throw err;
    })
  ).data;
}
