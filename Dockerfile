FROM node:lts
WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

EXPOSE 8200
CMD ["npm", "run", "docker-dev"]