import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
// https://vitejs.dev/config/

const headersLive = {
  "X-Frame-Options": "Deny",
};

export default defineConfig((args) => {
  const { command } = args;
  return {
    server: {
      port: 8200,
      server: {
        headers: command === "serve" ? headersLive : {},
      },
      hmr: {
        clientPort: 8200,
      },
      watch: {
        usePolling: true,
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@import "@/assets/styles/_variables.scss";`,
        },
      },
    },
    plugins: [vue()],
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
      },
    },
  };
});
